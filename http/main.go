// http project main.go
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
)

const MAX_PARRALLEL = 5

func getSearch() string {
	return "New York"
}

func getUrls() []string {
	return []string{
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/",
		"http://www.nycgo.com/",
		"https://www.newyorkpass.com/En/",
		"http://google.com.ua",
		"http://a.com",
		"https://golang.org/pkg/strings/#ToLowerSpecial",
		"http://www.thetimes.co.uk/"}
}

func isContains(text string, search string) bool {
	return strings.Contains(strings.ToLower(text), strings.ToLower(search))
}

func makeResponse(url string, wg *sync.WaitGroup, sema chan int) {
	sema <- 0
	defer func() { <-sema }()

	var response, error = http.Get(url)
	if error != nil {
		log.Print(error)
	} else {
		defer response.Body.Close()
		text, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Print(err)
		} else {
			fmt.Print(url, " - ", isContains(string(text), getSearch()), "\n")
		}
	}
	defer wg.Done()
}

func search(urls []string, searchIt string, wg *sync.WaitGroup) {
	fmt.Print("search: ", getSearch(), "\n\n")
	sema := make(chan int, MAX_PARRALLEL)

	for _, url := range urls {
		go makeResponse(url, wg, sema)
	}
}

func main() {
	var wg sync.WaitGroup

	wg.Add(len(getUrls()))
	search(getUrls(), getSearch(), &wg)
	wg.Wait()
}
