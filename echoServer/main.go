// echoServer project main.go
package main

import (
	"encoding/gob"
	"fmt"
	"log"
	"net"
	"os"
)

const _TAG = "echo"

func startServer(hostPort string) {
	clientListener, _ := net.Listen("tcp", hostPort)

	for {
		connection, err := clientListener.Accept()
		if err != nil {
			log.Fatalf("ERROR: failed to accept listener: %v", err)
		}

		defer connection.Close()
		fmt.Println(_TAG, "Accepted connection ", connection.RemoteAddr())

		var message string
		err = gob.NewDecoder(connection).Decode(&message)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Received: ", message)
		}

		err = gob.NewEncoder(connection).Encode("->" + message)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(_TAG, "send echo")
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("invalid args")
		os.Exit(1)
	}

	fmt.Println("listen: ", os.Args[1])
	startServer(os.Args[1])
}
