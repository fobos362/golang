package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"sync"
	"testing"
)

const _TAG = "tester:"

func connect() {
	log.Println(_TAG, "connecting to forward listener: ", os.Args[1])
	connectionToForward, err := net.Dial("tcp", os.Args[1])
	if err != nil {
		log.Fatalf(_TAG, "Dial failed: %v", err)
	}
	defer connectionToForward.Close()

	fmt.Fprint(connectionToForward, "testtesttest\n")
}

func listenClient(t *testing.T, wg *sync.WaitGroup) {
	clientListener, _ := net.Listen("tcp", os.Args[2])
	for {
		connection, err := clientListener.Accept()
		if err != nil {
			log.Fatalf("ERROR: failed to accept listener: %v", err)
		}

		log.Println(_TAG, "Accepted connection ", connection.RemoteAddr())
		message, _ := bufio.NewReader(connection).ReadString('\n')
		log.Println(_TAG, "Message Received:", string(message))

		if string(message) != "testtesttest\n" {
			t.Fatal("not reseived")
		} else {
			wg.Done()
			return
		}
	}
}

func TestMain(t *testing.T) {
	os.Args = []string{"main_test", "localhost:3335", "localhost:4446"}
	var wg sync.WaitGroup
	wg.Add(1)
	go listenClient(t, &wg)
	go startListener(&wg, os.Args[1])
	go connect()
	wg.Wait()
}
