// tcpPortForwarder project main.go
package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

const TAG = "forwarder:"
const MAX_PARRALLEL = 5

func startListener(wg *sync.WaitGroup, hostPort string) {
	fmt.Println(TAG, "start listener:", hostPort)
	listener, err := net.Listen("tcp", hostPort)
	if err != nil {
		fmt.Print("Failed to setup listener: %v\n", err)
		os.Exit(1)
		return
	}

	sema := make(chan int, MAX_PARRALLEL)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("ERROR: failed to accept listener: %v\n", err)
		}
		fmt.Println(TAG, "Accepted connection:", conn.RemoteAddr())
		sema <- 0
		go forward(conn, sema)
	}
	wg.Done()
}

func forward(conn net.Conn, sema chan int) {
	fmt.Println(TAG, "forward message from:", conn.RemoteAddr())
	client, err := net.DialTimeout("tcp", os.Args[2], time.Duration(5*time.Second))
	if err != nil {
		fmt.Println("forwarder: ", "Dial failed: %v\n", err)
		return
	}

	fmt.Println(TAG, "Connected to localhost", client.RemoteAddr())
	go func() {
		defer client.Close()
		defer conn.Close()
		io.Copy(client, conn)
		fmt.Println(TAG, "data send success\n", conn.LocalAddr())
	}()

	go func() {
		defer client.Close()
		defer conn.Close()
		io.Copy(conn, client)
		fmt.Println(TAG, "send response\n", conn.LocalAddr())
		<-sema
	}()
}

func main() {
	fmt.Print("start tcp forwarder\n")

	var wg sync.WaitGroup
	wg.Add(1)

	if len(os.Args) != 3 {
		fmt.Print("Usage %s listen:port forward:port\n", os.Args[0])
		os.Exit(1)
	}

	go startListener(&wg, os.Args[1])
	wg.Wait()
}
